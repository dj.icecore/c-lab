/************************************************************************************************
 * This program is permutation recognizer inside text file, followed by few rules:
 * 1. Receives 2 arguments from user that contains: file name(text file) and a string.
 * 2. Scan the file for permutations and the word it self.
 * 3. Print all permutations into a text file.
 * 4. Print to command line all permutations found in order it was found in the text file.
 ************************************************************************************************/
/*
 * TODO:
 * 
 * finish errors in main.
 * "if" before the loop is not finished.
 *  
 * */
#include "permut.h"


int permutComparing(char* permutationWord, char* fileWordsList, size_t wordsLen);

int main(int argc, char *argv[MAXIMUM_VAL]){
	/* File pointer */
	FILE *pFile;
	
	const char *fileName = "data.in"/*argv[1]*/;
	const char *word 	 = "chair"/*argv[2]*/;
	
	/* File open , file name is inserted first as 'argv' argument */
    pFile = fopen(fileName,"r");
    
    if(invalid_file(pFile)){
		return EXIT_FAILURE;
	}
		
    /* Word will be copied to arguments array to make permutation work */
    strcpy(fileWordsList,word);
    
    /* First word from file is fetched */
    fscanf(pFile, "%s", word);
	
	if( empty_file(pFile)){
		fclose(pFile);
		return EXIT_FAILURE;
	}
	qsort(fileWordsList, argumentLength, sizeof(char), comparing);
	
    do {
        int i;
        /* Check is length of characters of words that were found in file is matched to permutation word requirement */		
        argumentLength = strlen(fileWordsList);
        difference = argumentLength - strlen(argv[2]);			/* Variable for difference between words value */
        if (difference < 0) { continue; };						/* Check if word is valid by length */
        
		for (i=0; i <= difference; i++)
			if(permutComparing(permutationWord + i, fileWordsList, argumentLength))
			{
				printf("%.*s\n", (int)argumentLength, permutationWord + i);
				counter++;
			}
        printf("%.*s\n", (int)wordsLen,fileWordsList + i);
        counter++;
	} while (fscanf(pFile,"%s", argv[2]) != EOF);
	
	/* Print results */
	if (counter == 0)
		printf("Sorry, No permutations of %s found in %s.\n", argv[2], argv[1]);
	else
		printf("%i permutations of %s found in %s file.\n",(int)counter, argv[2], argv[1]);

    fclose(pFile);	/* Close file when work is finished */
    return EXIT_SUCCESS;
}

/* Comparing function */
int comparing(const void *x, const void *y)
{
    return (*(char *)x - *(char *)y);
}

/* Permutation comparing function */
int permutComparing(char *permutationWord, char *fileWordsList, size_t wordsLen){
    char sorted_word[MAXIMUM_VAL]; 							/* Local array for sorting words array */
	strcpy(sorted_word, fileWordsList);						/* Copy of sorted words */
	qsort(sorted_word, wordsLen, sizeof(char), comparing);	/* Sorting of words into sorted words array */
	
	if (memcmp(sorted_word, permutationWord, argumentLength) == 0)
	{
		return (1);											/* word is permutation of arguments */
	}
	return (0);  											/* word is not a permutation of arguments */
}

file_status invalid_argc(int argc){
	/* Check is number of arguments is valid*/
	if (argc != MAXIMUM_ARGUMENTS){
		if(argc < MAXIMUM_ARGUMENTS)
			fprintf(stderr, "Too low amount of arguments inserted, write %i arguments.\n", MAXIMUM_ARGUMENTS);					
		else
			fprintf(stderr, "You inserted too much arguments, maximum is %i.\n", MAXIMUM_ARGUMENTS);
		return SUCCESS;
	}	
	return FAIL;
}

file_status invalid_file(FILE *pFile){
	if (pFile == NULL){
		fprintf(stderr, "File cannot be opened!");
		return SUCCESS;
	}
	return FAIL;
}

file_status empty_file(FILE *pFile){
	if (feof(pFile))
	{
		fprintf(stderr, "File is empty!");
		return SUCCESS;
	}
	return FAIL;
}
